///////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME:            syscall.c
//
//    NAME & STUDENT NO.:   DAN STEER & 15006079       
//    NAME & STUDENT NO.:   ESTHER DALLEY & 16040610        
//    
//    First Edit:           01/11/2017
//    Last Edit:            06/12/2017
//
//    Description:          The system call handler is used to execute appropriate
//							code based on the system call given to the switch case.
//							For example, when the exit syscall is given to the 
//							switch, the appropriate code will be executed 
//							e.g. thread_exit()
//
///////////////////////////////////////////////////////////////////////////////////

#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include <string.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/init.h"
#include "devices/shutdown.h"
#include "threads/vaddr.h"
#include "devices/shutdown.h"
#include "userprog/process.h"
#include "filesys/filesys.h"


static void syscall_handler (struct intr_frame *);

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}


static void
syscall_handler (struct intr_frame *f UNUSED)
{
  uint32_t *p = f->esp; /*STORE SYSCALL NUMBER FROM CURRENT POSITION IN STACK*/
  printf("in system call: system call number: %d\n", *p);
  
  /*DEPENDING ON THE SYSCALL NUMBER, RUN DIFFERENT CASE*/
  switch(*p) {

	  /* Halt the operating system. */
	  case SYS_HALT: {  
		printf("IN SYS_HALT: %d\n", *p);
		shutdown_power_off (); /*CALL SHUTDOWN FUNCTION FROM shutdown.h */
		break;
	  }
	  
	  /* Terminate this process. */
	  case SYS_EXIT: { 
		printf("IN SYS_EXIT: %d\n", *p);
		thread_exit (); /*END THE CURRENT PROCESS AND RETURN AN EXIT CODE */
		break;
	  }

	  /* Start another process. */
	  case SYS_EXEC: { 
		printf("IN SYS_EXEC: %d\n", *p);
		break;
	  }	                        

	  /* Wait for a child process to die. */
	  case SYS_WAIT: { // 
		printf("SYS_WAIT\n");
		int fd = *(int *)(f->esp + 4);
		process_wait(fd);
		break;
	  }	

	  /* Create a file. */
	  case SYS_CREATE: {
		printf("SYS_CREATE\n");
		/* Get arguments: (name, size) */
		char *file_name = (char *)*(int *)(f->esp + 4);
		int file_size = *(int *)(f->esp + 8);
		/* Pass parameters into the filesys_create function, return result */
		if (filesys_create(*file_name, file_size)) {
			f->eax = true;
		} else {
			f->eax = false; }
		break;
	  }

	  /* Delete a file. */
	  case SYS_REMOVE: {
		printf("IN SYS_REMOVE: %d\n", *p);
		/* Get name of file */
		char *file_name = (char *)*(int *)(f->esp + 4);
		/* Pass parameter into the filesys_remove function, return result */
		if (filesys_remove(*file_name)) {
			f->eax = true;
		} else {
			f->eax = false; }
		break;
	  }	                     

	  /* Open a file. */
	  case SYS_OPEN: {
		printf("IN SYS_OPEN: %d\n", *p);
		char *file_name = *(int *)(f->esp + 4);
		int fd = filesys_open(file_name);
		if (fd != NULL) {
			f->eax = fd;
			//printf("%d\n", fd);
		} else {
			f->eax = -1; }
		//printf("%d\n", f->eax);
		break;
	  }

	  /* Obtain a file's size. */
	  case SYS_FILESIZE: { // 
		printf("IN SYS_FILESIZE: %d\n", *p);
		break;
	  }

	  /* Read from a file. */
	  case SYS_READ: { 
		printf("IN SYS_READ: %d\n", *p);
		int fd = *(int *)(f->esp + 4);
		void *buffer = *(char**)(f->esp + 8); /*GET STRING */
		unsigned size = *(unsigned *)(f->esp + 12); /* GET SIZE*/
		int read_size = process_read(fd, buffer, size); /* PRINT SIZE OF KEY PRESS & RETURN AS INT */
		f->eax = read_size; /* STORE THE READ SIZE IN THE ACCUMULATOR*/
		break;
	  }
	  
	  /* Write to a file. */
	  case SYS_WRITE: {
		printf("IN SYS_WRITE: %d\n", *p);
		int fd = *(int *)(f->esp + 4);
		void *buffer = *(char**)(f->esp + 8); /*GET STRING */
		unsigned size = *(unsigned *)(f->esp + 12); /* GET SIZE*/
		int written_size = process_write(fd, buffer, size); /*CALL FUNCTION TO PRINT STRING TO CONSOLE & RETURN SIZE OF WRITTEN STRING*/
		f->eax = written_size; /*STORE SIZE OF WRITTEN STRING IN THE ACCUMULATOR*/
		break;
	  }

	  /* Change position in a file. */
	  case SYS_SEEK: {
		printf("IN SYS_SEEK: %d\n", *p);
		break;
	  }	  

	  /* Report current position in a file. */
	  case SYS_TELL: { 
		printf("IN SYS_TELL: %d\n", *p);
		break;
	  }		  
	  
	  default: {
		printf("unhandled SYSCALL(%d)\n", *p);
		thread_exit ();
		break;
	  }
  }
}
