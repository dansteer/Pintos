#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"

tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit ();
void process_activate (void);
int process_write(int fd, void *buffer, unsigned size);
void extract_command_args(char *file_name, char* argv[], int *argc);

#endif /* userprog/process.h */
